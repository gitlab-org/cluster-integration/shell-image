# Web Shell Image

Minimal image with the following utilities installed:

- kubectl
- flux
- helm
- starship
- glab
- k9s
- bash

This image is designed to be deployed as part of GitLab's web terminal feature.

Its entrypoint is a login bash shell with a non-root user. The user does not
have sudo or root access.
