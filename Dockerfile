ARG ALPINE_VERSION
FROM alpine:edge
RUN echo 'https://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories
RUN apk update
RUN apk add kubectl \
  flux \
  helm \
  starship \
  glab \
  k9s \
  vim \
  nano \
  bash
RUN adduser -D user
USER user
WORKDIR /home/user
COPY bashrc .bashrc
COPY no-nerd-font.toml .config/starship.toml
ENTRYPOINT ["/bin/sh", "-lic"]
CMD ["bash"]
