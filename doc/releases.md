# Release Process

1. On the 15th, an automatic Slack notification reminds the Environments team to create a monthly release.
1. On the 15th, tag a new version that matches the upcoming GitLab minor version. E.g. If the upcoming milestone is 13.7,
   then tag `v13.7.0`.
1. Update the image used in
   `app/assets/javascripts/clusters/agents/components/web_terminal.vue`. (Not
   yet a real file)
